# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.pyson import Eval, If
from trytond.transaction import Transaction


class Lot(metaclass=PoolMeta):
    __name__ = 'stock.lot'

    @classmethod
    def get_quantity(cls, lots, name):
        "Return null instead of 0.0 if no locations in context"
        if not Transaction().context.get('locations'):
            return {}.fromkeys([l.id for l in lots], None)
        return super(Lot, cls).get_quantity(lots, name)


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    @classmethod
    def __setup__(cls):
        super(Move, cls).__setup__()
        cls.lot.context.update({
            'locations': [Eval('from_location')],
            'stock_date_end': If(Eval('effective_date'),
                    Eval('effective_date'),
                    If(Eval('planned_date'),
                        Eval('planned_date'),
                        Eval('_parent_shipment', {}).get('effective_date'))
                    )
        })
        if 'from_location' not in cls.lot.depends:
            cls.lot.depends.extend(['from_location', 'effective_date',
                'planned_date'])
        cls.lot.loading = 'lazy'

        if 'product' not in cls.lot.depends:
            cls.lot.depends.append('product')
        cls.lot.states['readonly'] |= ~Eval('product') | ~Eval('from_location')
